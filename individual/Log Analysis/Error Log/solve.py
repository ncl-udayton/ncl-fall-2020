from collections import defaultdict
import re

filere = re.compile('File does not exist: (.*)(,|$)')

scan = defaultdict(set)

with open('error.log.txt') as f:
	for line in f:
		if 'File does not exist' not in line: continue
		line = line[:-1]  # remove trailing new line
		ip = line.split(']')[2].split(' ')[2]
		match = filere.search(line)
		file = match.group(1)
		file = file.split(', referer')[0]

		scan[ip].add(file)

ip_with_most_uniq = max(scan, key=lambda x: len(scan[x]))

print("ip %s with %d unique files" % (ip_with_most_uniq, len(scan[ip_with_most_uniq])))
