# Error Log (Medium)

1. How many "File does not exist" errors are in the log?

```
$ cat error.log.txt | grep "File does not exist" | wc -l
65119
```

2. How many unique IP addresses are listed in the log?

```
$ cat error.log.txt | cut -d ']' -f 3 | cut -d ' ' -f 3 | sort | uniq | wc -l
8121
```

3. What IP address generated the most errors?

```
$ cat error.log.txt | cut -d ']' -f 3 | cut -d ' ' -f 3 | sort | uniq -c | sort | tail -1
  34533 88.80.10.1
```

4. What IP address scanned for the most unique files?

5. How many unique files (count different cases as different files) did the top unique file scanning IP address request?

These questions are easier to solve with a [script](/solve.py).

```
$ python solve.py
ip 176.53.21.162 with 1559 unique files
```

