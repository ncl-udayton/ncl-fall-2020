# Access Log (Easy)

1. How many requests were made to the server?

```
$ cat access_log.txt | wc -l
5880
```

2. How many requests were made for robots.txt?

```
cat access_log.txt | grep robots.txt | wc -l
791
```

3. What is the IP address of the device that made the most requests to the server?

```
cat access_log.txt | cut -d ' ' -f 1 | sort | uniq -c | sort | tail
     42 213.159.213.137
     44 104.148.105.5
     45 157.245.66.20
     51 60.191.38.77
     62 193.169.254.5
     64 100.35.75.170
     88 149.56.142.135
     97 87.250.244.11
    120 35.193.112.44
    727 162.251.20.188
```

4. How many Partial Content response codes did the server return?

```
$ cat access_log.txt | cut -d '"' -f 3 | cut -d ' ' -f 2 | sort | uniq -c | sort
      1 400
      4 206
     68 304
   2772 200
   3035 404
```

5. What URL returned a "bad request" error code?

```
$ cat access_log.txt | grep "\" 400"
182.100.67.238 - - [20/Oct/2019:10:59:08 +0200] "POST /utility/convert/index.php?a=config&source=d7.2_x2.0 HTTP/1.1" 400 226
```

