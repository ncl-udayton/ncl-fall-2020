# PayPal (Hard)

 An online retail vendor has had a purchase marked as fraudulent after it had been shipped. Investigate the transaction logs to find the anomaly.



1. What is the Merchant Id of the vendor?

`P3FSA9QMG5GZ4`

2. How much does the flat-rate shipping cost?

`5.99`

3. How many seconds elapsed from the first transaction to the last transaction?