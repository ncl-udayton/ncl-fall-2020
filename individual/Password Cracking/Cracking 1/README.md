# Cracking 1

 Our officers have obtained password dumps storing hacker passwords. After obtaining a few plaintext passwords, it appears that they overlap with the passwords from the rockyou breach.



First obtain a copy of the [rockyou wordlist](https://github.com/brannondorsey/naive-hashcat/releases/download/data/rockyou.txt).

Create a text file containing the hashes: [hashes.txt](/hashes.txt)

Run hashcat:

```
hashcat64.exe -a 0 -m 0 -w 4 --status -o ./results.txt ./hashes.txt ./rockyou.txt
```

The results should be written out to [results.txt](/results.txt)



