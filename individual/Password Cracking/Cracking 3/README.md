# Cracking 3 (Medium)

 Our officers have obtained password dumps storing hacker passwords. It appears that they are all in the format: "SKY-QQTY-" followed by 4 digits. Can you crack them?

Use [hashcat's]( https://hashcat.net/hashcat/) [mask attack]( https://hashcat.net/wiki/doku.php?id=mask_attack ) to generate all possible combinations of `SKY-QQTY-?d?d?d?d`.

```
hashcat64 -a 0 -m 3 -w 2 -o ./results.txt ./hashes.txt SKY-QQTY-?d?d?d?d
```

