# Codenames (Easy)

MECHANIC and HOOSIER are being transported from CALICO to ROADHOUSE via BAMBOO.



Searching the names leads to [the secret service codenames](https://wikivisually.com/wiki/Secret_Service_code_name).



1. What is the real name of the first person that is being transported?

`Jared Kushner`

2. What is the real name of the second person that is being transported?

`Mike Pence`

3. What is the real name of the location that the MVPs are being transported from?

`The Pentagon`

4. What is the real name of the location that the MVPs are being transported to?

`The Waldorf-Astoria Hotel`

5. What transportation is being used to move the MVPs?

`The Presidential Motorcade`