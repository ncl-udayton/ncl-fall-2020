# Printer (Medium)

 Analyze network traffic of a print request and see if you can extract the print job.



1. What is the Job Id of the print job that was sent to the printer?

Search for the `String` `job-id` in the `Packet list`

`16`

2. What is the MIME type of the file that was sent for printing?

`application/vnd.hp-PCL`

3. What is the IP address of the printer?

`10.10.10.251`

4. What is the version number of the interpreter that is displayed on the document that was sent to the printer?

Find the `Request (Print-Job)` packet. In the `IPP` section find the `Data` (should be 227161 bytes). Right click and `Export Packet Bytes`. Running file on the exported file tells us its `HP PCL printer data`. Use an [online PCL to PDF converter](https://www.pdfconvertonline.com/pcl-to-pdf-online.html) and open the resulting pdf.

` 3010 (81501) `

