# Service Scan (Easy)

 Test your understanding of port scanning by scanning **scanme9423.cityinthe.cloud** and answering these questions.



Usually a command line tool like nmap would be used, but you could also use a GUI like [zenmap](https://nmap.org/zenmap/).

```
nmap -sV -p 1-65535 -T4 -A -v -Pn scanme9423.cityinthe.cloud
```

1. What is the lowest open TCP port on the server?

`22`

2. What is the second lowest open TCP port on the server?

`80`

3. What is the third lowest open TCP port on the server?

`194`

4. What is the fourth lowest open TCP port on the server?

`6667`

5. What is the name of the software service running on the 3rd lowest port?

Loading ` http://scanme9423.cityinthe.cloud:194 ` in a browser opens a web service.

`Rocket Chat`

6. What is the "Instance ID" of the service running on the 3rd lowest TCP port?

From the nmap output we can see the `X-Instance-ID` header:

```
194/tcp  open   irc?
| fingerprint-strings: 
|   GetRequest, HTTPOptions: 
|     HTTP/1.1 200 OK
|     X-Instance-ID: 5oEfZ5aizdk8Ghm7R
```

`5oEfZ5aizdk8Ghm7R`

7. What is the name of the software service running on the 4th lowest TCP port?

nmap also reports this port as an irc service. Use a program like [Hex Chat](https://hexchat.github.io/) to connect to the service.

`InspIRCd`