# MongoDB (Hard)

 The Metropolis Police Department has identified a lead on potential MongoDB database cluster storing personal information stolen by the hacker group liber8tion. They tried to connect to **datastore.liber8tion.cityinthe.cloud** but have not had success, can you help them identify how to potentially access the database cluster?



Using [sublister](https://github.com/aboul3la/Sublist3r) we can see a list of all subdomains available.

```
areyouabot.cityinthe.cloud
blog.cityinthe.cloud
cityauth.cityinthe.cloud
elevate.cityinthe.cloud
game.cityinthe.cloud
hidden-files.cityinthe.cloud
hidden_files.cityinthe.cloud
hr.cityinthe.cloud
network.liber8tion.cityinthe.cloud
login.cityinthe.cloud
marketing.cityinthe.cloud
metro-clinic.cityinthe.cloud
metro-wallet.cityinthe.cloud
oracle.cityinthe.cloud
ports.cityinthe.cloud
scanme.cityinthe.cloud
scanme4173.cityinthe.cloud
spring2019ports.cityinthe.cloud
status.cityinthe.cloud
tacos.cityinthe.cloud
thalos.cityinthe.cloud
tom.cityinthe.cloud
tomsblog.cityinthe.cloud
transit.cityinthe.cloud
treasure.cityinthe.cloud
treasure2.cityinthe.cloud
treasure4173.cityinthe.cloud
treasure4173post.cityinthe.cloud
treasure80310382.cityinthe.cloud
treasurehunt.cityinthe.cloud
vuln_labs1.cityinthe.cloud
web.cityinthe.cloud
wicystreasure.cityinthe.cloud
```



`network.liber8tion.cityinthe.cloud` seems relevant. However, the copyright is 2018 so I'm guessing this is left over from another year.