### Decoding 1 (Easy)

1.  00110000 01100011 01100001 01110011 01101000 01100010 01100001 01101110 01101011 00111001 

[binary to ascii]( https://www.rapidtables.com/convert/number/binary-to-ascii.html)

`0cashbank9`

2. 3662656c6c746f776e34

[hex to ascii](https://www.rapidtables.com/convert/number/hex-to-ascii.html)

`6belltown4`

3. NTAwbXVzdGxvb2szMjE=

[base64 decode](https://www.base64decode.org/)

`500mustlook321`



### Decoding 2 (Easy)

1. Gurer ner svir pbagnvaref ehaavat

[rot 13](https://cryptii.com/pipes/caesar-cipher)

`There are five containers running`

2. Lzwjw sjw fafw UNWk xgj lzsl kwjnwj nwjkagf

[rot -18](https://cryptii.com/pipes/caesar-cipher)

`There are nine CVEs for that server version`



### Decoding 3 (Medium)

68 111 32 121 111 117 32 116 104 105 110 107 32 116 104 101 121 32 119 105 108 108 32 98 101 32 97 98 108 101 32 116 111 32 100 101 99 114 121 112 116 32 116 104 105 115 32 45 32 67 114 121 112 116 111 49 55 56 52 122

```pyth
>>> msg = [68, 111, 32, 121, 111, 117, 32, 116, 104, 105, 110, 107, 32, 116, 104, 101, 121, 32, 119, 105, 108, 108, 32, 98, 101, 32, 97, 98, 108, 101, 32, 116, 111, 32, 100, 101, 99, 114, 121, 112, 116, 32, 116, 104, 105, 115, 32, 45, 32, 67, 114, 121, 112, 116, 111, 49, 55, 56, 52, 122]
>>> print(''.join([chr(a) for a in msg]))
Do you think they will be able to decrypt this - Crypto1784z
```



### Decoding 4 (Medium)



### Decoding 5 (Hard)

[railfence ciphers](http://rumkin.com/tools/cipher/vigenere.php)

1. w     eytaihtkyddyuuet nrp h rhv?aeiosoctece

`what key did you use to encrypt the archive?`

2. s ersnusdoh y m kyee

`send me your ssh key`

