### Stego 1 (Easy)

![](https://d3oh9oliddps2p.cloudfront.net/5dba3db6d2993a37bf1751ac/5dba3dced2993a37bf1751b1?md5=6f5075bbf496d0b64dbf60c2bb234fc0&t=111)

This is just the first one so always start with just `strings`.

```
$ strings "Stego 1.jpg" | grep SKY
SKY-HONK-2651
```



### Stego 2 (Medium)

![](https://d3oh9oliddps2p.cloudfront.net/5dba3eccd2993a37bf1751bc/5dba4231d2993a37bf1751e1?md5=78193471e468b72a7b6219fa202c201f&t=111)

???
Use steghide http://steghide.sourceforge.net/documentation.php



### Stego (Hard)

![](https://d3oh9oliddps2p.cloudfront.net/5dba4136d2993a37bf1751d4/5dba4420d2993a37bf17520d?md5=65516a6d0f6404a213a21515d85cf6bc&t=111)



They indicate that there is more than one image in the file so `binwalk` it.

```
$ binwalk "Stego 3.jpg

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
402           0x192           Copyright string: "Copyright Apple Inc., 2017"
372014        0x5AD2E         JPEG image data, JFIF standard 1.01
372396        0x5AEAC         Copyright string: "Copyright (c) 1998 Hewlett-Packard Company"
434719        0x6A21F         JPEG image data, JFIF standard 1.01
559080        0x887E8         JPEG image data, JFIF standard 1.01
559110        0x88806         TIFF image data, big-endian, offset of first image directory: 8
595062        0x91476         JPEG image data, JFIF standard 1.01
675365        0xA4E25         JPEG image data, JFIF standard 1.01
```

1. How many unique images (excluding thumbnails, including the base image) can you find from this file?

`6`

2. What is the hidden flag in the file? (50 pts)

Extract all of the files from the image and you will see the flag in one of the images.

```
binwalk --dd=".*" "Stego 3.jpg"
```

