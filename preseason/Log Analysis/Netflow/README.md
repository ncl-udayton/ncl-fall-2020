# Netflow

1. How many total NetFlow records are there?

Idea: Count the rows 

``` cat netflow.txt | wc -l```

2. How many unique client IP addresses are there?

Idea: Extract the IP Address in column 5, count the unique items 


`cat netflow.txt | awk '{print $5}' | awk -F: '{print $1}' | sort | uniq  | wc -l`

3. What is the most frequent client IP address? 

Idea: Extract the IP Address in column 5, count the appearance of each IP

`cat netflow.txt | awk '{print $5}' | awk -F: '{print $1}' | sort | uniq  -c | sort -k1`
4. What IP address appeared exactly 10 times?
?

Idea: Extract the IP Address in column 5, count the appearance of each IP, find the IP appear 10 times
`cat netflow.txt | awk '{print $5}' | awk -F: '{print $1}' | sort | uniq  -c | sort -k1`

5. What does the S in the flags column stand for?

Idea: Something relates to the TCP state, Google:  APSF in Netflow

`Syn`

6. How many total S flags were there?

Idea: Find appearance of S in column 8


`cat netflow.txt | awk '{print $8}'  | grep S -c`
