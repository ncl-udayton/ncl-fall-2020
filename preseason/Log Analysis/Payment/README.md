# Payment

## This is XML format, However, it's not well-structured. Use AWK to filter
### This contains the requests to and response from the payment system and some another logs as well. 

1. How many transactions are contained in the log?

Idea:  Filter all logs response from payment system

``` cat payments.log  | grep  Response -c```

Result: ```192```

2. What was the largest purchase made in the log?


Idea : Analyze the Request payment in the field order Total

Sample command: `cat payments.log | grep Request | awk -F'currencyID="USD">' '{print $2}' | awk -F'</ebl:OrderTotal>' '{print $1}' | grep -v Amount | sort`


Result : ```998.6```


3. Which state made the most number of purchases?


Idea : Analyze the Request payment in the field State

Command: `cat payments.log | grep Request | grep  -i state | awk -F'StateOrProvince>' '{print $2}' | awk -F'<' '{print $1}' | sort  | uniq -c  | sort -k1`

Result: `MA`

