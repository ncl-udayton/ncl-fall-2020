# Scanner

## You can use Filter feature in Excel



1. How many files did the IDS process?

Idea: Count the lines, except the header.


Result: ```161```

2. How many days does this log span (inclusive, round to nearest whole number)?


Idea: Use Excel to calculate the MIN and MAX Time, differ these two values 

`13 days, 21:01:00`




3. What is the URL (without any query parameters) of the file that the IDS rejected?



Idea: use Excel to filter the column Action: Rejected

Result: ```http://r8---sn-a8au-p5qs.gvt1.com/edgedl/release2/zeirnkcni5cljoiivcv2uemx4yu6v5yn3bc8pdpb4doovg7qh6rilan8gw6cegeekb4caghglper5fmo9ena5eixsrcehhvchgu/54.0.2840.99_54.0.2840.71_chrome_updater.exe``` 


4. How many zip files did the IDS process?


Idea: use Excel to filter the column File Type: ZIP


Result: ```108```

5. How many unique server IP addresses were downloads made from?



Idea: Use Unique function in Excel to find unique IP in column Destination IP 

Excel query: ```=UNIQUE(F:F)```

Result: ```34```

6. What is the URL of the largest file downloaded?

Idea: Use Excel to sort the column File size


`http://download-cdn.gfe.nvidia.com/packages/DAO/production/21376336/00009766/0.dat`

