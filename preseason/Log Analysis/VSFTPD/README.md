# VSFTPD (Hard)

## Make sure you understand about FTP protocol





1. What IP address did "ftpuser" first log in from?


Idea: Because FTP Server will log the username and IP in one line, we use ```grep```  for searching. Since they are asking for First Login, we use ```head``` command. 

``` cat vsftpd.log | grep ftpuser | head```

Result: ```10.0.0.123```

2. What is the first directory that ftpuser created? 

Idea: We should to know the command to create a folder is MKDIR, search for it. Btw, it also appear in the previous command

`cat vsftpd.log | grep ftpuser | head`

OR 

`cat vsftpd.log | grep ftpuser | grep MKDIR`

Result : ```/home/ftpuser/TreeSizeFree```



3. What is the last directory that ftpuser created?


Idea: Same technique above

Result: ```/home/ftpuser/110D300S``` 


4. What file extension was the most commonly used by ftpuser?

Idea: Look for all commands by ftpuser and you will see the related files. 

`cat vsftpd.log | grep ftpuser`

Result: ```JPG```

5. What is the username of the other user in this log?


Idea: As we know in Question #1,  the command to login is : LOGIN, search for it you will know the other user. Of course, we should ignore the user : ftpuser

Command: `cat vsftpd.log | grep LOGIN | grep -v ftpuser`

Result: ```jimmy```

6. What is the IP address did this other user log in from?


Idea: From the previous command


`10.0.0.214`



7. How many total bytes did this other user upload?

Idea: Search for command UPLOAD by user jimmy, Use awk to SUM the columns with the size information

Step 1:  Look for the correct messsages : ```cat vsftpd.log | grep UPLOAD | grep jimmy```


Step 2 : Extract the column 15th ```cat vsftpd.log | grep UPLOAD | grep jimmy | awk '{print $15}' ```

Step 3:  Sum the  values in the column: ```cat vsftpd.log | grep UPLOAD | grep jimmy | awk '{print $15}' | awk '{s+=$1} END {print s}'```

8. How many total bytes did ftpuser upload?

Idea: Same logic above.

However, the way to extract the columns is different due to the file name of some file has spaces 

Step 1:  Look for the correct messsages : ```cat vsftpd.log | grep UPLOAD | grep ftpuser```

Step 2: Extract the column with size : ```cat vsftpd.log | grep UPLOAD | grep ftpuser | awk -F, '{print $3}' | awk '{print $1}'```

Step 3: Sum the values: ```cat vsftpd.log | grep UPLOAD | grep ftpuser | awk -F, '{print $3}' | awk '{print $1}'|awk '{s+=$1} END {print s}' ```

Result: ```13980839165```


Q9. How many total bytes did ftpuser download?

Idea:  Same logic as above, but we filter the DOWNLOAD messages. 

Result: ```6008032```

Q10. Identify the suspicious (login with no activity) login IP address in this log.

Idea: As we know either by reading or experimenting the log, we know that any user need to CONNECT before LOGIN, so use filter the CONNECT messages to get all IP address tried to connect. 

Step 1: Get all connected IP ```cat vsftpd.log | grep CONNECT```

Step 2: We knew the IP `10.0.0.123` was expected by ftpuser, so the other one is the one we are looking for. 

Result: `10.3.0.6`
