# Barcode

This is a barcode, use a scanner tool for scanning. 
We can use phone or use a website:  [https://online-barcode-reader.inliteresearch.com/]

## What format/type does the barcode use? (10 pts)

```Code39```


## What is the flag hidden in the barcode? (25 pts)

```SKY-UZLU-5635```