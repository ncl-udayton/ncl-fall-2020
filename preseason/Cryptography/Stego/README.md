### Stego 1 (Easy)

![](https://cyberskyline.com/artifact/5f7243e10e1ab04c9a52fcaa/5ed16df860cc3d09b5657720/5f847358068c327f5626624f/5acd1b091ec7ff7406852e51/5acd1b131ec7ff7406852e52/download?t=4)


???

### Stego 2 (Medium)

![](https://cyberskyline.com/artifact/5f7243e10e1ab04c9a52fcaa/5ed16df860cc3d09b5657720/5f847358068c327f5626624f/5adfa9f950884a2c9f6ce7ab/5adfab2150884a2c9f6ce7f0/download?t=4)


Extract all of the files from the image and you will see the flag in one of the images.

```
binwalk --extract --dd=".*" "Steg 2.jpg"
```