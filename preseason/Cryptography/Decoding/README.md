### Decoding 1 (Easy)

1.  00110100 01110111 01100101 01110011 01110100 01110000 01101100 01100001 01101110 00110011	

[binary to ascii]( https://www.rapidtables.com/convert/number/binary-to-ascii.html)


2. 373362696c6c7468656e3736

[hex to ascii](https://www.rapidtables.com/convert/number/hex-to-ascii.html)


3. NzdmaXJlYXJlYTg5

[base64 decode](https://www.base64decode.org/)




### Decoding 2 (Easy)

1. FXL-TGNX-8313

[caesar-cipher](https://www.dcode.fr/caesar-cipher)

Then look for the text starts with SKY-

2. IAO-WJQA-4837	

[caesar-cipher](https://www.dcode.fr/caesar-cipher)
Then look for the text starts with SKY-


### Decoding 3 (Medium)


|1 0 1 2 3 4 5|
|2 6 7 8 9 A B|
|3 C D E F G H|
|4 I J K L M N|
|5 O P Q R S T|
|6 U V W X Y Z|

Message: 54	33	33	65		52	65		61 22		54	15	64	45	15	33		55	65	45	33	33	65

```pyth
#Initialize the matrix using the above table
a =[
["0","1","2","3","4","5","6"],
["1","0","1","2","3","4","5"],
["2","6","7","8","9","A","B"],
["3","C","D","E","F","G","H"],
["4","I","J","K","L","M","N"],
["5","O","P","Q","R","S","T"],
["6","U","V","W","X","Y","Z"]
]

print(a[4][5],a[3][3],a[3][3],a[5][6]," ",a[2][5],a[5][6]," ",a[1][6],a[2][2],a[4][5],a[5][1],a[4][6],a[5][4],a[5][1],a[3][3]," ",a[5][5],a[5][6],a[5][4],a[3][3],a[3][3],a[5][6])

```



### Decoding 4 (Medium)

What is the plaintext of the message: 9 12-5-6-20 9-20 9-14 20-8-5 19-15-21-20-8-19-9-4-5 4-5-1-4 4-18-15-16 9-14 20-8-5 16-1-18-11? (25 pts)


```pyth

#Initialize the text
texts="9 12-5-6-20 9-20 9-14 20-8-5 19-15-21-20-8-19-9-4-5 4-5-1-4 4-18-15-16 9-14 20-8-5 16-1-18-11"

#Use for loop for each number in the text
for text in texts.split(' '):
    for c in text.split("-"):
        print(chr(96+int(c)), end ="")
    print( end =" ")
```