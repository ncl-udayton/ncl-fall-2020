# Basic Training

## Use wireshark

Q1. What is the IP address of the client machine?

Idea: Find requests packets, identify the source IP, such as packet No. 1

Result:  `172.16.102.130`



Q2. What is the default DNS resolver configured for this client?

Idea: find DNS query packets, identify the destination IP, such as packet No. 2

Result: `172.16.102.2`


Q3. What is the hostname of the first website this client visited?

Idea: Find the HTTP Requests, look for the Host name 

Idea 2: www.abc.com and abc.com are considered as 1 . 

Search query: `http `

Look in: HTTP requests from current machine: `172.16.102.130`, in section Hypertext Transfer Protocols

Result : `bing.com`

Q4. What is the hostname of the second website this client visited?

Idea: Same as above 

Result: `google.com`

Q5: What is the hostname of the third website this client visited?


Idea: Find the HTTP Requests, look for the Host name 

Idea 2: www.abc.com and abc.com are considered as 1 . 

Idea 3: No more HTTP packets, We can also look at the HTTPS packets as well.  But HTTPS packets does not provide raw information ? Actually yes, in the SSL information exchange. We need to look for SNI information. 


Look at the image : SNI.png

Result: `www.cyberskyline.com`







