# Split

## Using Wireshark


Q1. What port is the receiver listening on?

Idea: Local IP is `172.16.238.11`, Find the ports that data to this IP: 

Result: `59381`

Q2. How many TCP streams are present in the capture?

Results: `1121`

Q3. What is the flag that was sent?

Idea: get data from all streams


Command: 
```
for i in {0..1120};
do 
   tshark -r Split.pcap -q -z follow,tcp,ascii,$i | grep -v "==" | tail -1 >> output.txt

done
```

Idea 2: Get the output, base64 decode. Search for the key






