# Port Scan
## Use wireshark

Q1. What tool generated this traffic?

Idea: The information should be somewhere in the metadata of the file. 

Result:  `tcpdump`



Q2. How many hosts responded to the scan?

Idea: Hosts send packets to the local host are the one response to the scan 

Command: `tshark -r PortScan.pcap -2  -R "ip.dst==192.168.22.3"| awk '{print $3}' | sort -u| wc-l`

Result: `8`


Q3. What is the target subnet in CIDR notation?

Idea: Look for all IP Address and calculate the CIRD


Command: `shark -r PortScan.pcap -2  -R "ip.src==192.168.22.3"| awk '{print $5}'  | sort -u`


Result : `192.168.30.0/23`

Q4. What's an IP address that had VNC running?

Idea: Search for protocol vnc

Command : `shark -r PortScan.pcap -2  -R "vnc" | awk '{print $3}' | sort -u`
Result: `192.168.30.65` or `192.168.31.94`


Q5: What IP address had a mail server running?

Idea: Same as above, protocol = smtp


Command : `shark -r PortScan.pcap -2  -R "smtp" | awk '{print $3}' | sort -u`

Result: `192.168.31.94`

